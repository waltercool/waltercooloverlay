# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DB_VER="5.3"
CMAKE_MAKEFILE_GENERATOR="ninja"

inherit autotools bash-completion-r1 db-use desktop xdg-utils cmake

DESCRIPTION="Bitcoin Cash Node crypto-currency CLI + GUI"
HOMEPAGE="https://bitcoincashnode.org"
SRC_URI="
	https://gitlab.com/bitcoin-cash-node/bitcoin-cash-node/-/archive/v${PV}/bitcoin-cash-node-v${PV}.tar.gz
"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~arm ~arm64 ~ppc ~ppc64 ~x86 ~amd64-linux ~x86-linux"

IUSE="+asm dbus +qrcode +system-leveldb test upnp +wallet zeromq qt5"
RESTRICT="!test? ( test )"

RDEPEND="
	>=dev-libs/boost-1.52.0:=[threads(+)]
	>=dev-libs/libsecp256k1-0.22.4:=[recovery,schnorr]
	>=dev-libs/univalue-1.0.4:=
	qt5? (	dev-qt/qtcore:5
			dev-qt/qtgui:5
			dev-qt/qtnetwork:5
			dev-qt/qtwidgets:5
		)
	system-leveldb? ( virtual/bitcoin-leveldb )
	dbus? ( dev-qt/qtdbus:5 )
	dev-libs/libevent:=
	qrcode? (
		media-gfx/qrencode:=
	)
	upnp? ( >=net-libs/miniupnpc-1.9.20150916:= )
	wallet? ( sys-libs/db:$(db_ver_to_slot "${DB_VER}")=[cxx] )
	zeromq? ( net-libs/zeromq:= )
"
DEPEND="${RDEPEND}"
BDEPEND="
	>=sys-devel/autoconf-2.69
	>=sys-devel/automake-1.13
	qt5? ( dev-qt/linguist-tools:5 )
	!net-p2p/bitcoin-qt
	!net-p2p/bitcoin-cli
"

S="${WORKDIR}/bitcoin-cash-node-v${PV}"

src_prepare() {
	eapply_user

	#echo '#!/bin/true' >share/genbuild.sh || die
	rm -r src/secp256k1 || die
	# Remove internal secp256k1
	sed -i "s/add_subdirectory(secp256k1)//g" src/CMakeLists.txt

	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=(
		-DBerkeleyDB_VERSION=${DB_VER}.0
		-DBerkeleyDB_CXX_LIBRARY=/usr/lib64/libdb_cxx-${DB_VER}.so
		-DBUILD_SHARED_LIBS=OFF
		-DBUILD_BITCOIN_WALLET=$(usex wallet)
		-DBUILD_BITCOIN_QT=$(usex qt5)
		-DENABLE_UPNP=$(usex upnp)
		-DENABLE_QRCODE=$(usex qrcode)
		-DENABLE_DBUS_NOTIFICATIONS=$(usex dbus)
		-DBUILD_BITCOIN_ZMQ=$(usex zeromq)
		-DBUILD_ASERT_TEST_VECTORS=$(usex test)
		-DENABLE_PROPERTY_BASED_TESTS=$(usex test)
		-DCLIENT_VERSION_IS_RELEASE=ON
		-DCCACHE=OFF
	)
	cmake_src_configure
}

src_install() {
	cmake_src_install

	insinto /usr/share/icons/hicolor/scalable/apps/
	doins share/pixmaps/bitcoin128.png

	cp "${FILESDIR}/org.bitcoin.bitcoin-qt.desktop" "${T}" || die
	domenu "${T}/org.bitcoin.bitcoin-qt.desktop"
}
